import pandas as pd
from sqlalchemy import create_engine


class PostgresClient:
    def __init__(self, db, username, password, host, port):
        self.dialect = "postgresql"
        self.db = db
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self._engine = None
        self.db_type = "postgresql"

    def _get_engine(self):
        db_uri = f"{self.dialect}://{self.username}:{self.password}@{self.host}:{self.port}/{self.db}"
        if not self._engine:
            self._engine = create_engine(db_uri)
        return self._engine

    @staticmethod
    def _cursor_columns(cursor):
        if hasattr(cursor, "keys"):
            return cursor.keys()
        else:
            return [c[0] for c in cursor.description]

    def execute(self, sql, connection=None):
        if connection is None:
            connection = self._engine.connect()
        return connection.execute(sql)

    def data_insert_from_frame(
        self, df, table, if_exists="append", index=False, **kwargs
    ):
        connection = self._engine.connect()
        connection.insert_from_frame(
            df, table, if_exists=if_exists, index=index
        )

    def to_frame(self, *args, **kwargs):
        conn = self._get_engine().connect()
        cursor = conn.execute(*args, **kwargs)
        if not cursor:
            return
        data = cursor.fetchall()
        if data:
            df = pd.DataFrame(data, columns=self._cursor_columns(cursor))
        else:
            df = pd.DataFrame()
        return df


if __name__ == "__main__":
    postgres_cli = PostgresClient(
        "airflow", "airflow", "airflow", "localhost", 5432
    )
