"""DAG to get the most traded company between given options"""

import json
import logging
import math
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
import requests
from airflow.models import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.sql import SQLCheckOperator
from airflow.providers.slack.operators.slack_webhook import (
    SlackWebhookOperator,
)
#from decouple import config
from helpers import render_jinja_template
from jinja2 import Template
from postgres_cli import PostgresClient

logging.basicConfig(format="[%(levelname)s]: %(message)s", level=logging.DEBUG)

BASE_URL = "https://www.alphavantage.co/query"
API_KEY = "TFHNYCWBD71JBSON"
STOCK_FN = "TIME_SERIES_DAILY"

SQL_TABLE = "stocks_daily"

CUR_PATH = Path.cwd()
SQL_PATH = CUR_PATH / "dags" / "sql"

#SQL_DB = config("SQL_DB")
#POSTGRES_USER = config("POSTGRES_USER")
#POSTGRES_PASSWORD = config("POSTGRES_PASSWORD")
#POSTGRES_HOST = config("POSTGRES_HOST")
#POSTGRES_PORT = config("POSTGRES_PORT")

POSTGRES_USER='airflow'
POSTGRES_PASSWORD='airflow'
POSTGRES_DB='airflow'
POSTGRES_PORT=5432
POSTGRES_HOST='postgres'
SQL_DB='airflow'


def _create_table_if_not_exists():
    sql_cli = PostgresClient(
        SQL_DB, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT
    )
    sql_cli._get_engine()
    sql_create = render_jinja_template(SQL_TABLE, "create_table")
    sql_cli.execute(sql_create)


def _get_stock_data(stock_symbol, **context):
    date = context["ds"]  # read execution date from context
    end_point = (
        f"{BASE_URL}?function={STOCK_FN}&symbol={stock_symbol}"
        f"&apikey={API_KEY}&datatype=json"
    )
    print(f"Getting data from {end_point}...")

    # To avoid or capture failures during request
    try:
        r = requests.get(end_point)
    except requests.exceptions.RequestException as e:
        raise logging.error("Something bad happened in the request: %s", e)

    # We need to check if we didn't reach the limit
    if "Time Series (Daily)" in json.loads(r.content):
        data = json.loads(r.content)["Time Series (Daily)"]
        if date in data:
            day_data = data[date]
            avg_price = (
                float(day_data["2. high"]) + float(day_data["3. low"])
            ) / 2
            avg_num_trades = int(day_data["5. volume"]) / 1440
        else:
            avg_price = avg_num_trades = float("nan")
    else:
        # Branch were the limit is reached
        day_data = date
        avg_price = avg_num_trades = float("nan")
    return [date, stock_symbol, avg_price, avg_num_trades]


def _insert_daily_data(**context):
    task_instance = context["ti"]
    companies = ["apple", "tesla", "fb"]
    logging.info("Companies that will be saved: %s", companies)
    data = []
    # Get data from tasks
    logging.info("Starting to recover data from companies from other tasks...")
    data = [
        {
            "company": company,
            "data": [
                task_instance.xcom_pull(task_ids=f"get_daily_data_{company}")
            ],
        }
        for company in companies
    ]

    logging.info("Data about companies read...")

    logging.info("Starting to move info of each company to DataFrame...")
    # Create companies dataframe
    df_list = [
        {
            "company": companies[index],
            "data": pd.DataFrame(
                data[index]["data"],
                columns=["date", "symbol", "avg_price", "avg_num_trades"],
            ),
        }
        for index in range(len(companies))
    ]
    logging.info("Data from each company changed to DataFrame format")

    # Create df
    logging.info("Starting to create a single DataFrame")
    df = pd.DataFrame()
    for index in range(len(companies)):
        df = df.append(df_list[index]["data"], ignore_index=True)

    df["date"] = pd.to_datetime(df["date"])
    df["date"] = df["date"].dt.strftime("%Y-%m-%d")

    logging.info("DataFrame created...")
    try:
        logging.info("Starting to insert data into DB...")
        sql_cli = PostgresClient(
            SQL_DB,
            POSTGRES_USER,
            POSTGRES_PASSWORD,
            POSTGRES_HOST,
            POSTGRES_PORT,
        )
        sql_cli._get_engine()
        sql_cli.data_insert_from_frame(df, SQL_TABLE)
        logging.info("Information saved")
    except Exception as e:
        logging.info("Something bad happened: %s", e)


def _get_most_traded_report(**context):
    # Read execution date
    date = context["ds"]
    # Create the connection
    logging.info("Starting to get data from table %s...", SQL_TABLE)
    # Recover sql query
    sql_query = render_jinja_template(SQL_TABLE, "select_all")
    df = None
    sql_cli = PostgresClient(
        SQL_DB, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT
    )
    sql_cli._get_engine()
    try:
        logging.info("Query to run: %s", sql_query)
        df = sql_cli.to_frame(sql_query)
    except Exception as e:
        logging.info("Something bad happened: %s", e)
    logging.info("Data read from DB")

    logging.info("Starting to generate report...")
    max_avg_num_trades = None
    if df is not None:
        date_reports_df = df[df["date"] == date]

        # Get the max id
        idx_max = pd.to_numeric(date_reports_df["avg_num_trades"]).idxmax()

        company = ""
        avg = None
        # Construct message vars
        if isinstance(idx_max, (int, np.integer)) and not math.isnan(idx_max):
            # Select the row id
            max_avg_num_trades = date_reports_df.loc[idx_max]
            company = max_avg_num_trades["symbol"]
            avg = max_avg_num_trades["avg_num_trades"]
        else:
            max_avg_num_trades = None

        logging.info("Report created...")
    # Print the message
    if max_avg_num_trades is not None:
        return f"The company with the hightest average trade on {date} is {company} with {avg}"
    else:
        return f"The companies had the same average on {date} which was {max_avg_num_trades}"


def get_sql_check_file(name, **context):

    date = context["ds"]

    with open(SQL_PATH / f"{name}.sql") as file:
        logging.info("Starting to read query at: %s", SQL_PATH / f'{name}.sql')
        sql_query = file.read()
    # Read the template
    t = Template(sql_query)
    # Add the param
    render = t.render(ds=date)
    logging.info("Query read: %s", render)
    return render


default_args = {
    "owner": "mutt",
    "retries": 0,
    "start_date": datetime(2022, 1, 3),
}
with DAG(
    "stocks",
    default_args=default_args,
    schedule_interval="@monthly",
) as dag:
    create_table_if_not_exists = PythonOperator(
        task_id="create_table_if_not_exists",
        python_callable=_create_table_if_not_exists,
    )
    # Data from Apple
    get_daily_data_apple = PythonOperator(
        task_id="get_daily_data_apple",
        python_callable=_get_stock_data,
        op_args=["aapl"],
    )
    # Data from Tesla
    get_daily_data_tesla = PythonOperator(
        task_id="get_daily_data_tesla",
        python_callable=_get_stock_data,
        op_args=["tsla"],
    )
    # Data from Facebook
    get_daily_data_fb = PythonOperator(
        task_id="get_daily_data_fb",
        python_callable=_get_stock_data,
        op_args=["fb"],
    )

    # Add insert stock data
    insert_daily_data = PythonOperator(
        task_id="insert_daily_data",
        python_callable=_insert_daily_data,
        trigger_rule="all_done",
    )

    # SQL check
    sql_check_apple = SQLCheckOperator(
        task_id="sql_check_apple",
        sql="sql/sql_check.sql",
        params={"symbol": "aapl"},
        conn_id="postgres_conn",
    )
    sql_check_fb = SQLCheckOperator(
        task_id="sql_check_fb",
        sql="sql/sql_check.sql",
        params={"symbol": "fb"},
        conn_id="postgres_conn",
    )
    sql_check_tesla = SQLCheckOperator(
        task_id="sql_check_tesla",
        sql="sql/sql_check.sql",
        params={"symbol": "tesla"},
        conn_id="postgres_conn",
    )

    # Get most traded report
    do_most_traded_report = PythonOperator(
        task_id="do_most_traded_report",
        python_callable=_get_most_traded_report,
        trigger_rule="all_done",
    )

    (
        create_table_if_not_exists
        >> [get_daily_data_apple, get_daily_data_tesla, get_daily_data_fb]
        >> insert_daily_data
        >> [sql_check_apple, sql_check_fb, sql_check_tesla]
        >> do_most_traded_report
    )
