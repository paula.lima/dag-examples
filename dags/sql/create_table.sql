CREATE TABLE IF NOT EXISTS {{SQL_TABLE}} (
    date TEXT,
    symbol TEXT,
    avg_num_trades REAL,
    avg_price REAL,
    UNIQUE(date,symbol)
)
