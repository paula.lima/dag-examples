import logging
from pathlib import Path

from jinja2 import Template

logging.basicConfig(format="[%(levelname)s]: %(message)s", level=logging.DEBUG)

CUR_PATH = Path.cwd()
SQL_PATH = CUR_PATH / "dags" / "sql"


def render_jinja_template(sql_table, query_name):
    """Render sql queries using jinja2

    Args:
        query_name (str): is the sql file nanme
        params (str, optional): If the query requires params. Defaults to None.
    """
    logging.info("Starting to read and render query...")
    with open(SQL_PATH / f"{query_name}.sql") as file:
        logging.info(f"Starting to read query at: {SQL_PATH / f'{query_name}.sql'}")
        sql_query = file.read()

    # Recover the template
    t = Template(sql_query)
    # Add the param
    render = t.render(SQL_TABLE=sql_table)
    logging.info(f"Query read: {render}")
    return render
